export default class Component {
	tagName;
	attribute;
	children;
	constructor(tagName, attribute, children) {
		this.tagName = tagName;
		this.attribute = attribute;
		this.children = children;
	}
	renderChildren() {
		if (this.children instanceof Array) {
			return (this.children = this.children.join(''));
		} else {
			return this.children;
		}
	}

	render() {
		if (this.children == null) {
			return `<${this.tagName} ${this.renderAttribute()}/>`;
		} else {
			return `<${
				this.tagName
			} ${this.renderAttribute()}>${this.renderChildren()}</${this.tagName}>`;
		}
	}

	renderAttribute() {
		if (this.attribute != null) {
			return `${this.attribute.name}="${this.attribute.value}"`;
		} else {
			return ``;
		}
	}
}
