import Component from './Component.js';
export default class Img extends Component {
	constructor(img) {
		super(`img`, { name: `src`, value: `${img}` }, null);
	}
	render() {
		return super.render();
	}
}
